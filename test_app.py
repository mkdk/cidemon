import unittest
from app import _get_advise

class TestGetAdviseFunction(unittest.TestCase):

    def test_get_advise_from_remote(self):
        self.assertTrue(_get_advise() is not None)

if __name__ == '__main__':
    unittest.main()