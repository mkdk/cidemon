#!/bin/bash

cd /var/app

uwsgi --http :8080 --chdir /var/app --wsgi-file $WSGI_PATH --callable app --master --processes $UWSGI_NUM_PROCESSES --threads $UWSGI_NUM_THREADS --uid $UWSGI_UID --gid $UWSGI_GID

