import requests
import json
from flask import Flask, make_response, jsonify, render_template


app = Flask(__name__)


@app.route('/')
def render_static():
    """
    index html with advise
    :return:
    """
    advise = "Look ma, no hands!"
    data = _get_advise()
    if data:
        advise = data
    return render_template('index.html', advise=advise)


def _get_advise():
    """
    get advise from https://api.adviceslip.com/advice
    :return: advice string or None
    """
    r = requests.get("https://api.adviceslip.com/advice")
    if r.status_code != 200:
        return None
    data = r.json()
    return data["slip"].get("advice")


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
